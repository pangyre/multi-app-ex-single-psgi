#!perl
use strictures;
use utf8;
use Plack::Builder;
use Encode;
use Mojo::Server::PSGI;
use Plack::App::CGIBin;
use Plack::App::WrapCGI;
use Path::Tiny;

use lib "./MyCatalystApp/lib";
require MyCatalystApp;

my $script = path(__FILE__)->slurp_utf8;

my $root = sub {
    ( my $html = $script ) =~ s/\A.+__DATA__.//s;
    [ 200,
      [ "Content-Type" => "text/html; charset=utf-8" ],
      [ encode_utf8($html) ] ];
};

my $source = sub {
    [ 200,
      [ "Content-Type" => "text/plain; charset=utf-8" ],
      [ encode_utf8($script) ] ];
};

my $cat_app = MyCatalystApp->psgi_app;

my $mojo_app = eval {
    my $server = Mojo::Server::PSGI->new;
    $server->load_app("./app/mojo.pl");
    $server->to_psgi_app;
};

my $cgi_app = Plack::App::CGIBin
    ->new( root => "./cgi" )
    ->to_app;

my $php = Plack::App::WrapCGI
    ->new( script => "/usr/bin/php ./cgi/hello.php",
           execute => 1 )
    ->to_app;

my $python = Plack::App::WrapCGI
    ->new( script => "./cgi/python.cgi",
           execute => 1 )
    ->to_app;

my $ruby = Plack::App::WrapCGI
    ->new( script => "./cgi/ruby.cgi",
           execute => 1 )
    ->to_app;

my $hello_dot_c = Plack::App::WrapCGI
    ->new( script => "./HelloWorld",
           execute => 1 )
    ->to_app;

builder {
    enable "Rewrite", rules => sub {
        s,(?<=/cat)\z,/,; # Add trailing slash for Cat app’s root.
    };

    mount "/css" => Plack::App::File->new(file => "./static/css.css")->to_app;
    mount "/cat" => $cat_app;
    mount "/mojo" => $mojo_app;
    mount "/cgi" => $cgi_app;
    mount "/python" => $python;
    mount "/ruby" => $ruby;
    mount "/php" => $php;
    mount "/c" => $hello_dot_c;
    mount "/source" => $source;
    mount "/" => $root;
};

__DATA__
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Multiple Apps and Executables with One PSGI</title>
    <link rel="stylesheet" href="/css">
  </head>
  <body>
   <section class="content">
    <h1>Multiple executables and apps with one <abbr title="Perl Web Server Gateway Interface">PSGI</abbr> server</h1>
    <ul>
      <li><a href="/css">/css</a>
         <ul>
           <li>Serves a single static file using <a href="https://metacpan.org/pod/Plack::App::File">Plack::App::File</a></li>
         </ul>
      </li>
      <li><a href="/cat">/cat</a>
         <ul>
           <li>Bare bones toy <a href="https://metacpan.org/pod/Catalyst">Catalyst</a> application, no models or views.</li>
         </ul>
      </li>
      <li><a href="/mojo">/mojo</a>
         <ul>
           <li>Bare bones toy <a href="https://docs.mojolicious.org/Mojolicious/Lite">Mojolicious::Lite</a> application to show integration.</li>
         </ul>
      </li>
      <li><a href="/cgi/perl.cgi">/perl.cgi</a>
         <ul>
           <li>Perl old school CGI script with <a href="https://metacpan.org/pod/CGI">CGI.pm</a>.</li>
         </ul>
      </li>
      <li><a href="/python">/python</a>
         <ul>
           <li><a href="https://www.python.org/">Python</a> hello world harnessed as CGI.</li>
         </ul>
      </li>
      <li><a href="/ruby">/ruby</a>
         <ul>
           <li><a href="">Ruby</a> CGI hello world.</li>
         </ul>
      </li>
      <li><a href="/php">/php</a>
         <ul>
           <li><a href="https://www.php.net/">PHP</a> hello world.</li>
         </ul>
      </li>
      <li><a href="/c">/c</a>
         <ul>
           <li>Hello world in C.</li>
         </ul>
      </li>
      <li><a href="/source">/source</a>
         <ul>
           <li>Source code of this <abbr title="Perl Web Server Gateway Interface">PSGI</abbr> harness</li>
           <li>Made with <a href="https://metacpan.org/pod/Plack">Plack</a> and <a href="https://metacpan.org/pod/Plack::Builder">Plack::Builder</a> in particular.</li>
           <li><a href="https://bitbucket.org/pangyre/multi-app-ex-single-psgi/">Code repository</a>.</li>
         </ul>
      </li>
    </ul>
   </section>
  </body>
</html>
